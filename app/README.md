Google Cloud Infrastructure State - App Layer
=========

Ensure the state of the GCP Infrastructure App Layer.  


| App                    |
| -------                |
| GCE VM                 |
| Container on VM        |
| Helm Chart             |


- GCE VMs
  - "Baked" VMs that are deployed as Images.  OS changes should not be made post deploy in the production environment.
- Container on VM
  - A single container running on a single VM.  Prefers a container OS like [COS](https://cloud.google.com/container-optimized-os/) or [CoreOS](https://coreos.com/os/docs/latest/).
- Helm Chart
  - A [helm](https://helm.sh/) cart contains the Kubernetes configuration for a single or group of containers.


| Service                |
| -------                |
| GKE                    |
| Cloud Storage          |
| BigTable               |
| GAE                    |
| CloudSQL               |

| Foundation             |
| -------                |
| VPC                    |
| IAM                    |
| Security               |



This automation playbook is written in:

- [Ansible](https://docs.ansible.com/ansible/index.html) which uses Python to ensure a desired state is reached for the infrastructure on a single run.


Requirements
------------

To be able to put GCP into a state you must have one of these permissions:

- GCP credentials for that environment
- GCP service account access


Coding Standards & Naming conventions
-------------------------------------

Follow all the best practices outlined on the Ansible docs:

[Ansible Best Practices](https://docs.ansible.com/ansible/playbooks_best_practices.html)

[Ansible YAML Syntax](https://docs.ansible.com/ansible/YAMLSyntax.html)

[Terraform Best Practices](https://www.terraform.io/guides/running-terraform-in-automation.html)

Additional:

  - a role does one thing well
  - a single Terraform resource will not be made into a role
  - multiple Terraform resources that are dependent of each other will be grouped into roles
  - all files on disk that are managed by Ansible must be have a heading comment of
    `# {{ ansible_managed }}`
  - all files that are Terraform j2 templates must be prefaced with the label `terraform_`


Dependencies
------------

To use Ansible for GCP on OSX you must have the following installed:

- Ansible

`sudo pip install ansible` _recommended_

`brew install ansible`  

- GCP Cloud SDK

- apache-libcloud

`pip install --upgrade --trusted-host pypi.python.org apache-libcloud`

- google-auth

`pip install --upgrade google-auth --trusted-host pypi.python.org`

- google-auth-httplib2

`pip install --upgrade google-auth-httplib2 --trusted-host pypi.python.org`

To use Terraform for GCP on OSX you must have the following installed:

- Terraform

`brew install terraform`



License
-------

Apache 2.0

Author Information
------------------

chris L

[chris.livermore@dev9.com](chris.livermore@dev9.com)

byron L

[byron.lantzsch@dev9.com](byron.lantzsch@dev9.com)

omar J

[omar.juma@dev9.com](omar.juma@dev9.com)
