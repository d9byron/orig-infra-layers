Google Cloud Infrastructure State - Service Layer
=========

Ensure the state of the GCP Infrastructure Service Layer.

| App                    |
| -------                |
| GCE VM                 |
| Containers on VMs      |
| Helm Charts            |

| Service                |
| -------                |
| GKE                    |
| Cloud Storage          |
| BigTable               |
| GAE                    |
| CloudSQL               |

- Services are groupings of GCP products that are pre-defined with dependencies resolved.  
- Naming of groups will use intials of the products:
  - gke-cs-clsql = Google Kubernetes Engine configured to use Cloud Storage and a CloudSQL database.
  - gae-lb = A Google App Engine configured behind a Load Balancer
- The infrastructure as code when run will create a state of multiple services already configred to work with each other.

| Foundation             |
| -------                |
| VPC                    |
| IAM                    |
| Security               |

This automation playbook is written in:

- [Ansible](https://docs.ansible.com/ansible/index.html) which uses Python to ensure a desired state is reached for the infrastructure on a single run.

- [Terraform](https://www.terraform.io/) which uses Go to ensure a desired state is reached for the infrastructure in two runs.  The first run `terraform plan` is to query and establish a `state` file which is then used by `terraform apply` to ensure the state in the file is matching the infrastructure.


Requirements
------------

To be able to put GCP into a state you must have one of these permissions:

- GCP credentials for that environment
- GCP service account access


Coding Standards & Naming conventions
-------------------------------------

Follow all the best practices outlined on the Ansible docs:

[Ansible Best Practices](https://docs.ansible.com/ansible/playbooks_best_practices.html)

[Ansible YAML Syntax](https://docs.ansible.com/ansible/YAMLSyntax.html)

[Terraform Best Practices](https://www.terraform.io/guides/running-terraform-in-automation.html)

Additional:

  - a role does one thing well
  - a single Terraform resource will not be made into a role
  - multiple Terraform resources that are dependent of each other will be grouped into roles
  - all files on disk that are managed by Ansible must be have a heading comment of
    `# {{ ansible_managed }}`
  - all files that are Terraform j2 templates must be prefaced with the label `terraform_`


Dependencies
------------

To use Ansible for GCP on OSX you must have the following installed:

- Ansible

`sudo pip install ansible` _recommended_

`brew install ansible`  

- GCP Cloud SDK

- apache-libcloud

`pip install --upgrade --trusted-host pypi.python.org apache-libcloud`

- google-auth

`pip install --upgrade google-auth --trusted-host pypi.python.org`

- google-auth-httplib2

`pip install --upgrade google-auth-httplib2 --trusted-host pypi.python.org`

To use Terraform for GCP on OSX you must have the following installed:

- Terraform

`brew install terraform`



License
-------

Apache 2.0

Author Information
------------------

chris L

[chris.livermore@dev9.com](chris.livermore@dev9.com)

byron L

[byron.lantzsch@dev9.com](byron.lantzsch@dev9.com)

omar J

[omar.juma@dev9.com](omar.juma@dev9.com)
