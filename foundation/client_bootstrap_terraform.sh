#!/bin/bash

set -x

# based on "Managing GCP Projects with Terraform"
#      https://cloud.google.com/community/tutorials/managing-gcp-projects-with-terraform

###############################################################################
#
# Fill out the exported vars just below the comment
#
# get info to fill out the org id and billing account id with these commands
#
#     gcloud organizations list
#     gcloud beta billing accounts list
#
# You may want to choose a particular name for `TF_ADMIN_PRJ` instead of
# accepting the generated one here. It must be unique across all of gcloud or
# it will produce an error.
#

export TF_VAR_org_id=YOUR_ORG_ID
export TF_VAR_billing_account=YOUR_BILLING_ACCOUNT_ID
export TF_ADMIN_PRJ=${USER}-terraform-admin
export TF_CREDS=~/.config/gcloud/dev9-terraform-admin.json

###############################################################################

# Create the Terraform Admin Project
gcloud projects create ${TF_ADMIN_PRJ} --organization ${TF_VAR_org_id} --set-as-default
#gcloud beta billing projects link ${TF_ADMIN_PRJ} --billing-account ${TF_VAR_billing_account}

# Create the Terraform service account
gcloud iam service-accounts create terraform --display-name "Terraform admin account"
gcloud iam service-accounts keys create ${TF_CREDS} --iam-account terraform@${TF_ADMIN_PRJ}.iam.gserviceaccount.com

# Grant the service account permission to view the Admin Project and manage Cloud Storage
#gcloud projects add-iam-policy-binding ${TF_ADMIN_PRJ} --member serviceAccount:terraform@${TF_ADMIN_PRJ}.iam.gserviceaccount.com --role roles/viewer
#gcloud projects add-iam-policy-binding ${TF_ADMIN_PRJ} --member serviceAccount:terraform@${TF_ADMIN_PRJ}.iam.gserviceaccount.com --role roles/storage.admin

# Terraform performs requires a few APIs to be enabled
gcloud services enable cloudresourcemanager.googleapis.com
gcloud services enable cloudbilling.googleapis.com
gcloud services enable iam.googleapis.com
gcloud services enable compute.googleapis.com

set +x

cat <<HERE
NOTE THAT THIS MAY TAKE SOME TIME TO GO THROUGH

google knows that it just takes a while for everything to come up to speed
when you provision a project and bring all of this up

if the following commands fail, check the web console. maybe wait a bit and
try again.
HERE

set -x

# Grant the service account permission to create projects and assign billing
# accounts. `billing.user` would make more sense, but something about the
# gcloud API at the time of writing does not allow terraform to finalize the
# association of a billing account to a project without `billing.admin`.
gcloud organizations add-iam-policy-binding ${TF_VAR_org_id} --member serviceAccount:terraform@${TF_ADMIN_PRJ}.iam.gserviceaccount.com --role roles/billing.admin
gcloud organizations add-iam-policy-binding ${TF_VAR_org_id} --member serviceAccount:terraform@${TF_ADMIN_PRJ}.iam.gserviceaccount.com --role roles/owner
gcloud organizations add-iam-policy-binding ${TF_VAR_org_id} --member serviceAccount:terraform@${TF_ADMIN_PRJ}.iam.gserviceaccount.com --role roles/orgpolicy.policyAdmin
gcloud organizations add-iam-policy-binding ${TF_VAR_org_id} --member serviceAccount:terraform@${TF_ADMIN_PRJ}.iam.gserviceaccount.com --role roles/resourcemanager.folderAdmin
gcloud organizations add-iam-policy-binding ${TF_VAR_org_id} --member serviceAccount:terraform@${TF_ADMIN_PRJ}.iam.gserviceaccount.com --role roles/resourcemanager.folderIamAdmin
gcloud organizations add-iam-policy-binding ${TF_VAR_org_id} --member serviceAccount:terraform@${TF_ADMIN_PRJ}.iam.gserviceaccount.com --role roles/resourcemanager.projectCreator
gcloud organizations add-iam-policy-binding ${TF_VAR_org_id} --member serviceAccount:terraform@${TF_ADMIN_PRJ}.iam.gserviceaccount.com --role roles/resourcemanager.projectDeleter
gcloud organizations add-iam-policy-binding ${TF_VAR_org_id} --member serviceAccount:terraform@${TF_ADMIN_PRJ}.iam.gserviceaccount.com --role roles/resourcemanager.lienModifier

set +x

cat <<HERE

HERE
