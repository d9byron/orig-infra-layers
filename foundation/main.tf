## ASSUMPTIONS:
#
# dev, stage, and prod projects DO NOT already exist. same goes for folders

# this needs to be filled out
variable "organization_numeric_id" { default = 933250405420 }

variable "billing_account_number" { }

data "google_billing_account" "client_billing_acct" {
  # using billing account (id number) instead of display name b/c it's
  # possible to edit the display name and make things "interesting"
  billing_account = "${var.billing_account_number}"
  open = true
}


# if you have run `gcloud auth application-default login` AND creds are
# available directly from your account email (vs a service account) then
# configuring the credentials file isn't necessary. otherwise, here it is
provider "google" {
  # credentials = "${file("../creds.d/dev_creds.json")}"
}

##### ALL PARAMETERIZATION ABOVE HERE

resource "google_folder" "dev9_handoff" {
  display_name = "dev9_handoff"
  parent       = "organizations/${var.organization_numeric_id}"
}

/*
Note that the various environments are not looped here. This is done with
the idea that different customers will likely have some variety in what
environments they want, and also the IAM setups on each. Default to giving
us the ability to do everything everywhere though. This is easy to remove
when the time comes for offboarding.

This _could_ turn into a module, but as soon as it did we would have to make
exceptions and alterations left, right, and center.
*/

resource "google_folder" "dev_env" {
  display_name = "dev"
  parent       = "${google_folder.dev9_handoff.name}"
}

resource "google_folder" "stage_env" {
  display_name = "stage"
  parent       = "${google_folder.dev9_handoff.name}"
}

resource "google_folder" "production_env" {
  display_name = "prod"
  parent       = "${google_folder.dev9_handoff.name}"
}

resource "google_project" "dev" {
  name = "development"
  project_id = "dev-${random_integer.project_random_number.result}"
  folder_id  = "${google_folder.dev_env.name}"
  billing_account = "${data.google_billing_account.client_billing_acct.id}"
}

resource "google_project" "stage" {
  name = "staging"
  project_id = "stage-${random_integer.project_random_number.result}"
  folder_id  = "${google_folder.stage_env.name}"
  billing_account = "${data.google_billing_account.client_billing_acct.id}"
}

resource "google_project" "production" {
  name = "production"
  project_id = "prod-${random_integer.project_random_number.result}"
  folder_id  = "${google_folder.production_env.name}"
  billing_account = "${data.google_billing_account.client_billing_acct.id}"
}

resource "google_project_services" "dev" {
  project  = "${google_project.dev.id}"
  services = ["cloudresourcemanager.googleapis.com", "cloudbilling.googleapis.com", "iam.googleapis.com", "compute.googleapis.com"]
}

resource "google_project_services" "stage" {
  project  = "${google_project.stage.id}"
  services = ["cloudresourcemanager.googleapis.com", "cloudbilling.googleapis.com", "iam.googleapis.com", "compute.googleapis.com"]
}

resource "google_project_services" "production" {
  project  = "${google_project.production.id}"
  services = ["cloudresourcemanager.googleapis.com", "cloudbilling.googleapis.com", "iam.googleapis.com", "compute.googleapis.com"]
}

resource "random_integer" "project_random_number" {
  # this is dumb, but it'd be great to stick to large numbers to avoid
  # collisions. sacrifice 10%
  min     = 10000
  max     = 99999
}

resource "google_folder_iam_policy" "folder_admin_policy" {
  folder     = "${google_folder.dev9_handoff.name}"
  policy_data = "${data.google_iam_policy.dev9_gcp_development.policy_data}"
}

data "google_iam_policy" "dev9_gcp_development" {
  # note: getting a mailing list would simplify this and make admin transitive
  #       from gsuite, which is great unless we don't want the b/c high touch
  binding {
    role = "roles/owner"

    members = [
      "user:byron.lantzsch@dev9.com",
      "user:chris.livermore@dev9.com",
      "user:mike.ensor@dev9.com",
      "user:omar.juma@dev9.com",
    ]
  }
}

output "organizations" {
  value = "${var.organization_numeric_id}"
}

output "project_random_number" {
  value = "${random_integer.project_random_number.result}"
}
