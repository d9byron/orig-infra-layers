Expectations and Strategic Approach
===================================

Best practice suggests that most of our work is isolated from any infra a
client might have already, and that any work a client might do in their cloud
while an engagement is in progress is also isolated from our work. This
prevents possible confusion. Under normal conditions, Dev9 work will occur
within a GCloud orgaization folder. When the engangement is over the client has
two options to off-board Dev9. The client can remove us from a single place in
their gcloud (the org folder). Alternatively, they can move projects into
another folder of the clients chosing in order to remove our IAM configurations
and apply different IAM configurations.

This approach keeps administrative effort low while still giving Dev9 the
access it needs to produce infra. It is also straightforward and transparent,
and makes it clear that when we are off-boarded from a project we no longer
need to be concerned with accidently taking action in someone elses cloud.


Avenues for onboarding Dev9 into Client Gcloud
==============================================

The person we work with on their end will  need relatively high IAM perms. See
the section [Client configures a Dev9 gcloud account]() for the minimum perms
they will need. More access is fine.

In order of least to most involvement by Dev9:

1. [Client runs foundation terraform]()
2. [Client runs bootstrap script]()
3. [Client configures a Dev9 gcloud account]() to be able run the foundation

Notes and expectations for each option follows.


### Client runs foundation terraform ###

This is the easiet for us. We can hand them the terraform in this directory
and all they have to do is adujst the variables default values in the `tf` file
or pass them into Terraform using one of the methods [here](https://www.terraform.io/docs/configuration/variables.html).


### Client runs bootstrap script ###

The client will run the bootstrap script and then hand us the JSON file
containing information to authenticate as the newly created service account.

It's good practice to encrypt this file before sending it over the wire.

Straightforward encryption options:
1. We have keybase.io accounts
2. `gpg --armor --symmetric $FILE_NAME` with a password they provide to us over
   the phone. Once they have `gpg` this is quite straightforward. They will
   send us the `account.json.asc` file which is produced.
3. Standard `gpg` keyswap
4. No encryption. The client sends us the creds, we run the foundation quickly,
   then ask them to delete the account _key_ *not* the service account itself.

A password and the content it encrypts should never go over the wire together.


### Client configures a Dev9 gcloud account ###

We can walk them through the webconsole to add an account and IAM.

They will need to apply the following to one of our accounts:

* roles/billing.admin
* roles/owner
* roles/orgpolicy.policyAdmin
* roles/resourcemanager.folderAdmin
* roles/resourcemanager.folderIamAdmin
* roles/resourcemanager.projectCreator
* roles/resourcemanager.projectDeleter

This is technically wider than needed assuming there are _no_ issues. Having
the extra bit makes it less likely we have to go back and forth with them.

Terraform Notes
===============

### Application Default Credentials ###

Somewhat ironically, application default credentials are not available by default.

In the event that a user has multiple accounts (as seen by `gcloud auth list`) they would need to ensure that the correct account is set as active. This is configured with `gcloud config set account $ACCOUNT`.

When the correct account is set run `gcloud auth application-default login`.

[This terraform github issue](https://github.com/terraform-providers/terraform-provider-google/issues/611#issuecomment-351836546) has context, framing, and details.


### Using a Service Account Key File in Terraform ###

Then you can point the terraform provider to the creds file as seen [here](https://github.com/dev9com/GCPInfrastructureState/blob/e1ee05bc7ee6a168b6ea4d10fe12ad223eb78626/foundation/main.tf#L12). The path may need to be adjusted.

General Note
============

Developing clear expectations about how GCloud works with a _completely_ fresh
account and organization is a curious process since getting a completely clean
view into what this looks like can only happen once per acount. Please take
good notes on anything which deviates from this doc and put any new info in here.

