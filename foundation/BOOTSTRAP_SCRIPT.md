Purpose
=======

This bootstrap script creates an isolated user who has the ability to build
the foundational infrasctructure where Dev9 will work. After this script is run
there will be a creds file which needs to be given to dev9, or optionally fed
into Terraform when the client opts run that on their own.

Prior to Running
================

Before the script is run it needs some info. Specifically, it will need the
client's organization ID and the client's billing account ID. The gcloud
command provides this information when the following is run:

    gcloud organizations list
    gcloud beta billing accounts list

Billing information is generally quite priviledged, and not all gcloud users in
the client's organization will have the ability to see it. It is implied by
several other roles, but when none of these roles are assigned to the user who
will run the script they will need at least `roles/billing.user` to view the
info.

It may be useful to choose a particular name for `TF_ADMIN_PRJ` instead of 
accepting the generated one here, in particular if the generated project name
is already in use. The project name _must_ be unique across _all_ of gcloud.

Nothing in the script should need to change outside the `export`-ed variables
between the lines of `#` towards the top of the file unless Google moves a
command around, e.g. from `alpha` to `beta`, etc.

Running the script
==================
The script will take a few minutes to execute. It should not need any input in
order to complete. If it does hit an error, please send the terminal content
of the script command and all of it's output back to the Dev9 contact.

When the script is finished there will be a json file with Gcloud creds in the
location `~/.config/gcloud/dev9-terraform-admin.json`, or to the location the
`TF_CREDS` variable was changed to.


